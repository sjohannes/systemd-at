# systemd-at

Implementation of the [Unix `at(1)` utility](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/at.html) that uses systemd timers.


## Compatibility

| Behavior | POSIX `at` | Debian `at` | systemd-at |
| - | - | - | - |
| Multiple users share the same jobs | Yes | Yes | No, systemd-at requires and uses the systemd user daemon |
| `at.allow` and  `at.deny` support | Yes | Yes | No
| `-m` (mail) support | Yes | Yes | No |
| `-q` (queue selection) support | Yes | Yes | No |
| `-r` (remove) support | Yes | No, uses `-d` instead | Yes |
| Local datetime is stored with timezone at creation | Yes | ? | No, local datetime is checked during creation but passed to systemd as naive datetime |
| Local datetime respects `TZ` environment variable | Yes | ? | No, only local (naive) and UTC timezones are supported, and the latter must be specified in the timespec |
| `-t` supports seconds | Yes | Yes | Yes |
| `-t` supports `Z` suffix to indicate UTC | No | No | Yes, to compensate for lack of `TZ` environment variable support
| `-l` output shows the queue name and job creator | No | Yes | No |
| `-l` datetime is like `date +"%a %b %e %T %Y"` | Yes | Yes | No, the format used is `YYYY-MM-DD hh:mm:ss[ UTC]` |
| `-l` datetime is in local time | Yes | Yes | No, UTC datetime is not converted to local time |

For the timespec syntax compatibility, see the [`unix-at-parser`](unix-at-parser) readme.


## Contributing

When contributing to this repository:

You agree to release any contribution you make to the `unix-at-parser` crate under the Apache License version 2.0.

You agree to release any contribution you make to other locations in this repository under the GNU General Public License version 3.0 or later.
