// Copyright (C) 2020  Johannes Sasongko <sasongko@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod cli;
mod util;

use std::{
    fs::OpenOptions,
    io::{stderr, stdout, Write},
    path::{Path, PathBuf},
};
use unix_at_parser::DateTimeTz;

#[repr(i32)]
#[derive(Debug, PartialEq)]
pub enum Error {
    /// Error parsing command line.
    OptionError = 1,
    /// Fatal error when dealing with stdin/stdout/stderr.
    StdIOError = 2,
    /// Any other error.
    JobError = 3,
}

impl Into<i32> for Error {
    fn into(self) -> i32 {
        self as i32
    }
}

struct ServiceValues {
    cwd: PathBuf,
    job: PathBuf,
    service: PathBuf,
    timer: PathBuf,
    umask: Vec<u8>,
}

fn get_base_data_dir() -> Result<PathBuf, Error> {
    dirs::data_local_dir().ok_or_else(|| {
        let _ = writeln!(stderr(), "error: Failed getting data directory");
        Error::JobError
    })
}

fn prepare_data_dir(path: impl AsRef<std::path::Path>) -> Option<()> {
    match util::get_dir_creator().create(&path) {
        Ok(()) => Some(()),
        Err(e) => {
            let _ = writeln!(
                stderr(),
                "error: Failed creating file: {}: {}",
                path.as_ref().to_string_lossy(),
                e,
            );
            None
        }
    }
}

fn step_copy_script(
    op: &cli::AddOp,
    file_creator: &OpenOptions,
    job_path: impl AsRef<Path>,
) -> Result<(), Error> {
    use std::io::{stdin, Read};
    match &op.file {
        Some(path) => {
            || -> Result<(), std::io::Error> {
                let mut src = std::fs::File::open(&path)?;
                let mut dest = file_creator.open(job_path.as_ref())?;
                std::io::copy(&mut src, &mut dest)?;
                Ok(())
            }()
            .map_err(|e| {
                let _ = writeln!(stderr(), "error: Failed copying job file: {}", e);
                Error::JobError
            })?;
        }
        None => {
            // NOTE: Don't use std::io::copy. The user could be entering the
            // script on the terminal and interrupting it with Ctrl+C, in which
            // case we shouldn't write anything.
            let mut input = Vec::<u8>::new();
            stdin().read_to_end(&mut input).map_err(|e| {
                let _ = writeln!(stderr(), "error: Failed reading stdin: {}", e);
                Error::StdIOError
            })?;
            file_creator
                .open(job_path.as_ref())
                .and_then(|mut dest| dest.write_all(&input))
                .map_err(|e| {
                    let _ = writeln!(stderr(), "error: Failed writing job file: {}", e);
                    Error::JobError
                })?;
        }
    }
    Ok(())
}

fn handle_add_op(op: cli::AddOp) -> Result<(), Error> {
    let time = step_parse_time(&op)?;
    let systemd_calendar = util::to_systemd_calendar(&time);
    if atty::is(atty::Stream::Stdin) {
        let _ = writeln!(stdout(), "Job time: {}\nScript: ", systemd_calendar);
    }

    let mut rng = nanorand::WyRand::new();
    let job_id = util::create_job_id(&time, &util::create_job_tag(&mut rng));

    let file_creator = util::get_file_creator();

    let base_data_dir = get_base_data_dir()?;
    let data_dir = base_data_dir.join("systemd-at");
    prepare_data_dir(&data_dir).ok_or_else(|| {
        let _ = writeln!(stderr(), "error: Failed creating data directory");
        Error::JobError
    })?;
    let job_path = data_dir.join(&job_id);

    let unit_dir = base_data_dir.join("systemd").join("user");
    prepare_data_dir(&unit_dir).ok_or_else(|| {
        let _ = writeln!(stderr(), "error: Failed creating systemd unit directory");
        Error::JobError
    })?;
    let service_unit_path = unit_dir.join(format!("{}.service", job_id));
    let timer_unit_path = unit_dir.join(format!("{}.timer", job_id));

    let cwd = std::env::current_dir().map_err(|e| {
        let _ = writeln!(
            stderr(),
            "error: Failed getting current working directory: {}",
            e,
        );
        Error::JobError
    })?;
    let umask = util::get_umask();

    let service_values = ServiceValues {
        cwd,
        job: job_path,
        service: service_unit_path,
        timer: timer_unit_path,
        umask,
    };

    step_copy_script(&op, &file_creator, &service_values.job)?;
    step_write_service(&service_values, &file_creator)?;
    step_write_timer(&file_creator, &service_values.timer, &systemd_calendar)?;

    let _ = writeln!(stderr(), "{}", job_id);
    Ok(())
}

fn handle_list_op(op: cli::ListOp) -> Result<(), Error> {
    use util::ToBytes;
    let data_dir = get_base_data_dir()?.join("systemd-at");
    // TODO: Check that this doesn't exhaust the iterator.
    if op.job_ids.is_empty() {
        for file in std::fs::read_dir(data_dir).map_err(|_| Error::StdIOError)? {
            let file = if let Ok(f) = file { f } else { continue };
            let job_id = file.file_name();
            let time = util::parse_time_from_job_id(&job_id.to_bytes());
            let time = if let Some(t) = time { t } else { continue };
            if std::fs::File::open(file.path()).is_err() {
                continue;
            };
            let stdout = stdout();
            let mut stdout = stdout.lock();
            stdout
                .write(&job_id.to_bytes())
                .and_then(|_| writeln!(stdout, "\t{}", util::to_systemd_calendar(&time)))
                .map_err(|_| Error::StdIOError)?;
        }
    } else {
        let mut errored = false;
        for ref job_id in op.job_ids {
            if !util::path_is_name_only(job_id) {
                // Prevent directory traversal
                let _ = writeln!(
                    stderr(),
                    "error: Invalid job ID: {}",
                    job_id.to_string_lossy(),
                );
                errored = true;
                continue;
            }
            let time = util::parse_time_from_job_id(&job_id.to_bytes());
            let time = if let Some(t) = time { t } else { continue };
            if std::fs::File::open(data_dir.join(job_id)).is_err() {
                let _ = writeln!(
                    stderr(),
                    "error: Failed reading job: {}",
                    job_id.to_string_lossy(),
                );
                errored = true;
                continue;
            }
            let stdout = stdout();
            let mut stdout = stdout.lock();
            stdout
                .write(&job_id.to_bytes())
                .and_then(|_| writeln!(stdout, "\t{}", util::to_systemd_calendar(&time)))
                .map_err(|_| Error::StdIOError)?;
        }
        if errored {
            return Err(Error::JobError);
        }
    }
    Ok(())
}

fn handle_remove_op(op: cli::RemoveOp) -> Result<(), Error> {
    let base_data_dir = get_base_data_dir()?;
    let data_dir = base_data_dir.join("systemd-at");
    let unit_dir = base_data_dir.join("systemd").join("user");
    let mut errored = false;
    for ref job_id in op.job_ids {
        if !util::path_is_name_only(job_id) {
            // Prevent directory traversal
            let _ = writeln!(
                stderr(),
                "error: Invalid job ID: {}",
                job_id.to_string_lossy(),
            );
            errored = true;
            continue;
        }
        let job_path = data_dir.join(job_id);
        if !job_path.exists() {
            let _ = writeln!(
                stderr(),
                "error: Job not found: {}",
                job_id.to_string_lossy(),
            );
            errored = true;
            continue;
        }
        // TODO: Stop the timer
        let timer_unit = util::path_join_and_append(&unit_dir, job_id, ".timer");
        let service_unit = util::path_join_and_append(&unit_dir, job_id, ".service");
        let mut remove = |p: &Path| {
            if let Err(e) = std::fs::remove_file(p) {
                let _ = writeln!(
                    stderr(),
                    "error: Failed removing file: {}: {}",
                    p.to_string_lossy(),
                    e,
                );
                errored = true;
            }
        };
        remove(&timer_unit);
        remove(&service_unit);
        remove(&job_path);
    }
    if errored {
        Err(Error::JobError)
    } else {
        Ok(())
    }
}

fn step_parse_time(op: &cli::AddOp) -> Result<DateTimeTz, Error> {
    use unix_at_parser::{chrono, parse_timespec, parse_touch_t};
    use util::ToBytes;

    let time = op
        .time
        .iter()
        .map(|s| s.to_bytes())
        .collect::<Vec<_>>()
        .join(&b' ');
    match op.time_format {
        cli::TimeFormat::At => parse_timespec(&time, &DateTimeTz::Local(chrono::Local::now())),
        cli::TimeFormat::TouchT => {
            // Without TZ env var support, there is normally no way to specify the
            // timezone for -t. Here we add nonstandard support for Z suffix.
            let (time, now) = if time.ends_with(b"Z") {
                (
                    &time[..(time.len() - 1)],
                    DateTimeTz::Utc(chrono::Utc::now()),
                )
            } else {
                (&*time, DateTimeTz::Local(chrono::Local::now()))
            };
            parse_touch_t(&time, &now)
        }
    }
    .map_err(|_| {
        let _ = writeln!(
            stderr(),
            "error: Invalid time: {}",
            String::from_utf8_lossy(&time),
        );
        Error::OptionError
    })
}

fn step_write_service(sv: &ServiceValues, file_creator: &OpenOptions) -> Result<(), Error> {
    use util::ToBytes;

    /// Converts &[u8] into possibly invalid &str, for use in formatting.
    fn sfuu(s: &[u8]) -> &str {
        unsafe { std::str::from_utf8_unchecked(s) }
    }

    // TODO: Escape (if possible)
    let cwd = sv.cwd.as_os_str().to_bytes();
    let env = util::systemd_escape_env_vars(std::env::vars_os());
    let job = sv.job.as_os_str().to_bytes();
    let service = sv.service.as_os_str().to_bytes();
    let shell = util::get_shell();
    let shell = shell.to_bytes();
    let timer = sv.timer.as_os_str().to_bytes();

    let mut f = file_creator.open(&sv.service).map_err(|e| {
        let _ = writeln!(
            stderr(),
            "error: Failed creating systemd service unit: {}",
            e,
        );
        Error::JobError
    })?;
    write!(
        f,
        concat!(
            "[Unit]\n",
            "Description = systemd-at job runner\n",
            "\n",
            "[Service]\n",
            "ExecStart = {shell} {job}\n",
            "ExecStartPost = -/bin/rm -f {timer}\n",
            "ExecStartPost = -/bin/rm -f {service}\n",
            "ExecStartPost = -/bin/rm -f {job}\n",
            "WorkingDirectory = {cwd}\n",
            "UMask = {umask}\n",
            "Environment = {env}\n",
        ),
        cwd = sfuu(&cwd),
        env = sfuu(&env),
        job = sfuu(&job),
        service = sfuu(&service),
        shell = sfuu(&shell),
        timer = sfuu(&timer),
        umask = sfuu(&sv.umask),
    )
    .map_err(|e| {
        let _ = writeln!(
            stderr(),
            "error: Failed writing systemd service unit: {}",
            e,
        );
        Error::JobError
    })?;

    Ok(())
}

fn step_write_timer(
    file_creator: &OpenOptions,
    path: impl AsRef<Path>,
    calendar: impl AsRef<str>,
) -> Result<(), Error> {
    let mut f = file_creator.open(path.as_ref()).map_err(|e| {
        let _ = writeln!(
            stderr(),
            "error: Failed creating systemd service unit: {}",
            e,
        );
        Error::JobError
    })?;
    write!(
        f,
        concat!(
            "[Unit]\n",
            "Description = systemd-at job runner\n",
            "\n",
            "[Timer]\n",
            "OnCalendar = {cal}\n",
            "\n",
            "[Install]\n",
            "WantedBy = timers.target\n",
        ),
        cal = calendar.as_ref(),
    )
    .map_err(|e| {
        let _ = writeln!(
            stderr(),
            "error: Failed writing systemd service unit: {}",
            e,
        );
        Error::JobError
    })?;

    Ok(())
}

pub fn main() -> Result<(), Error> {
    let op = cli::parse_cli(std::env::args_os().skip(1)).map_err(|e| {
        let stderr = stderr();
        let mut stderr = stderr.lock();
        let _ = writeln!(stderr, "error: {}\n", e);
        cli::print_usage(&mut stderr);
        Error::OptionError
    })?;

    match op {
        cli::Op::Help => {
            let stdout = stdout();
            let mut stdout = stdout.lock();
            cli::print_usage(&mut stdout);
            cli::print_help(&mut stdout);
            Ok(())
        }
        cli::Op::Version => {
            cli::print_version(&mut stdout());
            Ok(())
        }
        cli::Op::Add(op) => handle_add_op(op),
        cli::Op::List(op) => handle_list_op(op),
        cli::Op::Remove(op) => handle_remove_op(op),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_t_supports_z() {
        use unix_at_parser::chrono::{Local, TimeZone, Utc};
        let op = cli::AddOp {
            file: None,
            time: vec!["200010301234.56".into()],
            time_format: cli::TimeFormat::TouchT,
        };
        assert_eq!(
            step_parse_time(&op),
            Ok(DateTimeTz::Local(
                Local.ymd(2000, 10, 30).and_hms(12, 34, 56)
            )),
        );
        let op = cli::AddOp {
            file: None,
            time: vec!["200010301234.56Z".into()],
            time_format: cli::TimeFormat::TouchT,
        };
        assert_eq!(
            step_parse_time(&op),
            Ok(DateTimeTz::Utc(Utc.ymd(2000, 10, 30).and_hms(12, 34, 56))),
        );
    }
}
