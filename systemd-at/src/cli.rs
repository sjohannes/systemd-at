// Copyright (C) 2020  Johannes Sasongko <sasongko@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{ffi::OsString, path::PathBuf};

#[derive(Debug, PartialEq)]
pub enum Op {
    Add(AddOp),
    Help,
    List(ListOp),
    Remove(RemoveOp),
    Version,
}

#[derive(Debug, PartialEq)]
pub struct AddOp {
    pub file: Option<PathBuf>,
    pub time_format: TimeFormat,
    pub time: Vec<OsString>,
}

#[derive(Debug, PartialEq)]
pub struct ListOp {
    pub job_ids: Vec<OsString>,
}

#[derive(Debug, PartialEq)]
pub struct RemoveOp {
    pub job_ids: Vec<OsString>,
}

#[derive(Debug, PartialEq)]
pub enum TimeFormat {
    At,
    TouchT,
}

#[derive(Debug)]
enum PartialOp {
    Add(PartialAddOp),
    Help,
    List,
    Remove,
    Version,
}

#[derive(Debug)]
struct PartialAddOp {
    file: Option<PathBuf>,
    time_format: TimeFormat,
}

pub fn print_usage(f: &mut impl std::io::Write) {
    let _ = write!(
        f,
        concat!(
            "Usage: systemd-at [-f FILE] [-t] TIME...\n",
            "  or:  systemd-at -l [JOB]...\n",
            "  or:  systemd-at -r JOB...\n",
            "  or:  systemd-at {{-h|--help|-V|--version}}\n",
        ),
    );
}

pub fn print_help(f: &mut impl std::io::Write) {
    let _ = write!(
        f,
        concat!(
            "Run script at a scheduled time.\n",
            "\n",
            "Arguments:\n",
            "  TIME           when to run the job (see at(1p))\n",
            "  JOB            job identifier\n",
            "  -f FILE        populate job script from FILE instead of stdin\n",
            "  -h, --help     print this help\n",
            "  -l             print information about the specified jobs\n",
            "  -t             TIME is in [[yy]yy]mmddHHMM[.SS] format\n",
            "  -r             remove the specified jobs\n",
            "  -V, --version  print program name and version\n",
        ),
    );
}

pub fn print_version(f: &mut impl std::io::Write) {
    let _ = writeln!(
        f,
        "{} {}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION"),
    );
}

pub fn parse_cli(mut args: impl Iterator<Item = OsString>) -> Result<Op, String> {
    use crate::util::ToBytes;

    let mut op: Option<PartialOp> = None;
    let mut positional: Vec<OsString> = Vec::new();
    while let Some(arg) = args.next() {
        if arg == "-h" || arg == "--help" {
            op = Some(PartialOp::Help);
        } else if arg == "-V" || arg == "--version" {
            op = Some(PartialOp::Version);
        } else if arg == "-f" {
            let file = args.next().ok_or_else(|| "-f expects a path".to_owned())?;
            match op {
                None => {
                    op = Some(PartialOp::Add(PartialAddOp {
                        file: Some(PathBuf::from(file)),
                        time_format: TimeFormat::At,
                    }));
                }
                Some(PartialOp::Add(ref mut addop)) => {
                    addop.file = Some(PathBuf::from(file));
                }
                _ => return Err("-f is incompatible with this operation".to_owned()),
            }
        } else if arg == "-t" {
            match op {
                None => {
                    op = Some(PartialOp::Add(PartialAddOp {
                        file: None,
                        time_format: TimeFormat::TouchT,
                    }));
                }
                Some(PartialOp::Add(ref mut op)) => {
                    op.time_format = TimeFormat::TouchT;
                }
                _ => return Err("-t is incompatible with this operation".to_owned()),
            }
        } else if arg == "-l" {
            if op.is_some() {
                return Err("-l is incompatible with this operation".to_owned());
            }
            op = Some(PartialOp::List);
        } else if arg == "-r" {
            if op.is_some() {
                return Err("-r is incompatible with this operation".to_owned());
            }
            op = Some(PartialOp::Remove);
        } else if arg == "--" {
            if op.is_none() {
                op = Some(PartialOp::Add(PartialAddOp {
                    file: None,
                    time_format: TimeFormat::At,
                }))
            }
            break;
        } else if !arg.is_empty() && arg.to_bytes()[0] == b'-' {
            return Err(format!("Invalid option: {:?}", arg));
        } else {
            if op.is_none() {
                op = Some(PartialOp::Add(PartialAddOp {
                    file: None,
                    time_format: TimeFormat::At,
                }))
            }
            positional.push(arg);
        }
    }
    positional.extend(args); // Leftovers after --

    let op = match op {
        None => {
            return Err("Missing arguments".to_owned());
        }
        Some(PartialOp::Add(op)) => {
            if positional.is_empty() {
                return Err("Missing time".to_owned());
            }
            Op::Add(AddOp {
                file: op.file,
                time: positional,
                time_format: op.time_format,
            })
        }
        Some(PartialOp::Remove) => {
            if positional.is_empty() {
                return Err("Missing job ID".to_owned());
            }
            Op::Remove(RemoveOp {
                job_ids: positional,
            })
        }
        Some(PartialOp::List) => Op::List(ListOp {
            job_ids: positional,
        }),
        Some(PartialOp::Help) => Op::Help,
        Some(PartialOp::Version) => Op::Version,
    };
    Ok(op)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn ok<'a>(args: &[&str]) -> Op {
        let args = args.iter().map(|s| s.into()).collect::<Vec<OsString>>();
        let op = parse_cli(args.into_iter());
        op.unwrap()
    }

    fn ok_add(args: &[&str]) -> AddOp {
        if let Op::Add(op) = ok(args) {
            op
        } else {
            panic!("op is not add");
        }
    }

    fn ok_list(args: &[&str]) -> ListOp {
        if let Op::List(op) = ok(args) {
            op
        } else {
            panic!("op is not list");
        }
    }

    fn ok_remove(args: &[&str]) -> RemoveOp {
        if let Op::Remove(op) = ok(args) {
            op
        } else {
            panic!("op is not remove");
        }
    }

    fn err(args: &[&str]) {
        let args = args.iter().map(|s| s.into()).collect::<Vec<OsString>>();
        let op = parse_cli(args.into_iter());
        op.unwrap_err();
    }

    #[test]
    fn test_add_timespec() {
        let op = ok_add(&["now"]);
        assert_eq!(op.time_format, TimeFormat::At);
        assert_eq!(op.time, &["now"]);

        let op = ok_add(&["1234", "today"]);
        assert_eq!(op.time_format, TimeFormat::At);
        assert_eq!(op.time, &["1234", "today"]);

        let op = ok_add(&["-f", "ba.sh", "1234", "today"]);
        assert_eq!(op.time_format, TimeFormat::At);
        assert_eq!(op.file, Some("ba.sh".into()));
        assert_eq!(op.time, &["1234", "today"]);

        err(&[]);
    }

    #[test]
    fn test_add_touch_t() {
        let op = ok_add(&["-t", "10301234"]);
        assert_eq!(op.time_format, TimeFormat::TouchT);
        assert_eq!(op.time, &["10301234"]);

        let op = ok_add(&["-f", "ba.sh", "-t", "10301234"]);
        assert_eq!(op.time_format, TimeFormat::TouchT);
        assert_eq!(op.file, Some("ba.sh".into()));
        assert_eq!(op.time, &["10301234"]);

        err(&["-t"]);
    }

    #[test]
    fn test_help() {
        assert_eq!(ok(&["-h"]), Op::Help);
        assert_eq!(ok(&["--help"]), Op::Help);
    }

    #[test]
    fn test_list() {
        ok_list(&["-l"]);

        let op = ok_list(&["-l", "at-1"]);
        assert_eq!(op.job_ids, &["at-1"]);

        let op = ok_list(&["-l", "at-1", "at-2"]);
        assert_eq!(op.job_ids, &["at-1", "at-2"]);

        let op = ok_list(&["-l", "at-1", "--", "at-2"]);
        assert_eq!(op.job_ids, &["at-1", "at-2"]);

        err(&["-t", "10301234", "-l", "at-1"]);
        err(&["-l", "at-1", "-r", "at-1"]);
        err(&["-r", "at-1", "-l", "at-1"]);
    }

    #[test]
    fn test_remove() {
        let op = ok_remove(&["-r", "at-1"]);
        assert_eq!(op.job_ids, &["at-1"]);

        let op = ok_remove(&["-r", "at-1", "at-2"]);
        assert_eq!(op.job_ids, &["at-1", "at-2"]);

        let op = ok_remove(&["-r", "at-1", "--", "at-2"]);
        assert_eq!(op.job_ids, &["at-1", "at-2"]);

        err(&["-r"]);
        err(&["-t", "10301234", "-r", "at-1"]);
    }

    #[test]
    fn test_version() {
        assert_eq!(ok(&["-V"]), Op::Version);
        assert_eq!(ok(&["--version"]), Op::Version);
    }
}
