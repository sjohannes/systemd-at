// Copyright (C) 2020  Johannes Sasongko <sasongko@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::{
    borrow::Cow,
    ffi::{OsStr, OsString},
    fs::{DirBuilder, OpenOptions},
    path::Path,
};
use unix_at_parser::DateTimeTz;

pub trait ToBytes {
    fn to_bytes(&self) -> Cow<'_, [u8]>;
}

impl ToBytes for OsStr {
    fn to_bytes(&self) -> Cow<'_, [u8]> {
        #[cfg(unix)]
        {
            use std::os::unix::ffi::OsStrExt;
            Cow::Borrowed(self.as_bytes())
        }
        #[cfg(not(unix))]
        match self.to_string_lossy() {
            Cow::Borrowed(s) => Cow::Borrowed(s.as_bytes()),
            Cow::Owned(s) => Cow::Owned(s.into_bytes()),
        }
    }
}

/// Creates a job ID based on the datetime and tag.
///
/// This is always in the format `at-yyyymmddHHMM.SS[Z]-tag`.
pub fn create_job_id(dt: &DateTimeTz, tag: &str) -> String {
    use unix_at_parser::chrono::{Datelike, Timelike};
    match dt {
        DateTimeTz::Local(dt) => format!(
            "at-{:04}{:02}{:02}{:02}{:02}.{:02}-{}",
            dt.year(),
            dt.month(),
            dt.day(),
            dt.hour(),
            dt.minute(),
            dt.second(),
            tag,
        ),
        DateTimeTz::Utc(dt) => format!(
            "at-{:04}{:02}{:02}{:02}{:02}.{:02}Z-{}",
            dt.year(),
            dt.month(),
            dt.day(),
            dt.hour(),
            dt.minute(),
            dt.second(),
            tag,
        ),
    }
}

/// Creates a random string to add to the end of a job ID.
pub fn create_job_tag(rng: &mut impl nanorand::RNG) -> String {
    let tag: u32 = rng.generate();
    format!("{:08x}", tag)
}

/// DirBuilder + recursive + mode 0700.
pub fn get_dir_creator() -> DirBuilder {
    let mut builder = DirBuilder::new();
    builder.recursive(true);
    #[cfg(unix)]
    {
        use std::os::unix::fs::DirBuilderExt;
        builder.mode(0o700);
    }
    builder
}

/// OpenOptions + create_new + write + mode 0600.
pub fn get_file_creator() -> OpenOptions {
    let mut opts = OpenOptions::new();
    opts.create_new(true).write(true);
    #[cfg(unix)]
    {
        use std::os::unix::fs::OpenOptionsExt;
        opts.mode(0o600);
    }
    opts
}

/// The SHELL environment variable value, or /bin/sh.
pub fn get_shell() -> OsString {
    std::env::var_os("SHELL").unwrap_or_else(|| "/bin/sh".into())
}

/// Returns the file mode creation mask (as octal string).
///
/// On Linux this function is correct and safe.
/// If reading/parsing /proc/self/status fails, it returns 0077.
///
/// On other Unix-like OSes this function is correct and safe if the first call
/// does not overlap with another call or with a file creation.
///
/// On non-Unix-like OSes this function just returns 0077.
pub fn get_umask() -> Vec<u8> {
    #[cfg(target_os = "linux")]
    {
        const PREFIX: &str = "Umask:\t";
        || -> Option<Vec<u8>> {
            use std::io::BufRead;
            let f = std::fs::File::open("/proc/self/status").ok()?;
            let reader = std::io::BufReader::new(f);
            reader.lines().find_map(|line| match line {
                Ok(l) if l.starts_with(PREFIX) => Some(l.into_bytes()[PREFIX.len()..].to_vec()),
                _ => None,
            })
        }()
        .unwrap_or(b"0077".to_vec())
    }
    #[cfg(all(unix, not(target_os = "linux")))]
    {
        static mut UMASK: Option<u32> = None;
        let umask = unsafe {
            UMASK.unwrap_or_else(|| {
                let umask = libc::umask(0o077);
                libc::umask(umask);
                UMASK = Some(umask);
                umask
            })
        };
        format!("0{:03o}", umask).into_bytes()
    }
    #[cfg(not(unix))]
    {
        b"0077".to_vec()
    }
}

/// Gets the datetime out of a job ID.
///
/// Returns None on invalid job ID or invalid datetime.
pub fn parse_time_from_job_id(job_id: &[u8]) -> Option<DateTimeTz> {
    use unix_at_parser::{
        chrono::{Local, Utc},
        parse_touch_t,
    };
    if !job_id.starts_with(b"at-") {
        return None;
    }
    let time_tag = &job_id[3..];
    let tagpos = (0..time_tag.len()).find(|&i| time_tag[i] == b'-')?;
    let mut time = &time_tag[..tagpos];
    let now = if time.ends_with(b"Z") {
        time = &time[..(time.len() - 1)];
        DateTimeTz::Utc(Utc::now())
    } else {
        DateTimeTz::Local(Local::now())
    };
    parse_touch_t(time, &now).ok()
}

/// Checks that a path only contains a name and nothing else.
pub fn path_is_name_only(path: impl AsRef<Path>) -> bool {
    let mut comps = path.as_ref().components();
    if let Some(comp) = comps.next() {
        if comps.next().is_none() {
            if let std::path::Component::Normal(_) = comp {
                return true;
            }
        }
    }
    false
}

/// Returns a path in the form `{base}/{part1}{part2}`.
pub fn path_join_and_append(
    base: impl AsRef<Path>,
    part1: impl AsRef<OsStr>,
    part2: impl AsRef<OsStr>,
) -> std::path::PathBuf {
    let mut name = OsString::with_capacity(part1.as_ref().len() + part2.as_ref().len());
    name.push(part1);
    name.push(part2);
    base.as_ref().join(name)
}

/// Calculates the number of extra bytes required to escape a bytestring for
/// systemd's Environment= directive.
fn systemd_env_count_extra(string: &[u8]) -> usize {
    string
        .iter()
        .filter(|c| matches!(c, b'"' | b'\\' | b'\n'))
        .count()
}

/// Escapes a bytestring for systemd's Environment= directive.
fn systemd_env_write(string: &[u8], dest: &mut impl std::io::Write) {
    for c in string {
        let _ = match c {
            b'"' => dest.write_all(br#"\""#),
            b'\\' => dest.write_all(br#"\\"#),
            b'\n' => dest.write_all(br#"\n"#),
            _ => dest.write_all(&[*c]),
        };
    }
}

/// Creates a bytestring containing all environment variables for systemd's
/// Environment= directive.
pub fn systemd_escape_env_vars<I, S>(vars: I) -> Vec<u8>
where
    I: std::iter::IntoIterator<Item = (S, S)>,
    S: AsRef<OsStr>,
{
    let vars: Vec<_> = vars
        .into_iter()
        .map(|(k, v)| {
            let k = k.as_ref().to_bytes();
            let v = v.as_ref().to_bytes();
            let k_extra = systemd_env_count_extra(&k);
            let v_extra = systemd_env_count_extra(&v);
            let result_len = k.len() + v.len() + k_extra + v_extra + 3;
            let mut result_buf: Vec<u8> = Vec::with_capacity(result_len);
            let mut result = std::io::Cursor::new(&mut result_buf);
            use std::io::Write;
            let _ = result.write_all(b"\"");
            if k_extra == 0 {
                let _ = result.write_all(&k);
            } else {
                systemd_env_write(&k, &mut result);
            }
            let _ = result.write_all(b"=");
            if v_extra == 0 {
                let _ = result.write_all(&v);
            } else {
                systemd_env_write(&v, &mut result);
            }
            let _ = result.write_all(b"\"");
            debug_assert_eq!(result_len, result_buf.len());
            result_buf
        })
        .collect();
    let result_len = if vars.is_empty() {
        0
    } else {
        vars.iter().map(|s| s.len()).sum::<usize>() + (vars.len() - 1)
    };
    let mut result: Vec<u8> = Vec::with_capacity(result_len);
    for (i, var) in vars.iter().enumerate() {
        if i != 0 {
            result.push(b' ');
        }
        result.extend(var);
    }
    debug_assert_eq!(result_len, result.len());
    debug_assert_eq!(result_len, result.capacity());
    result
}

/// Converts a DateTimeTz for systemd OnCalendar= directive.
pub fn to_systemd_calendar(dt: &DateTimeTz) -> String {
    use unix_at_parser::chrono::{Datelike, Timelike};
    match dt {
        DateTimeTz::Local(dt) => format!(
            "{:04}-{:02}-{:02} {:02}:{:02}:{:02}",
            dt.year(),
            dt.month(),
            dt.day(),
            dt.hour(),
            dt.minute(),
            dt.second(),
        ),
        DateTimeTz::Utc(dt) => format!(
            "{:04}-{:02}-{:02} {:02}:{:02}:{:02} UTC",
            dt.year(),
            dt.month(),
            dt.day(),
            dt.hour(),
            dt.minute(),
            dt.second(),
        ),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_job_tag() {
        let mut rng = nanorand::WyRand::new();

        let tags: std::collections::HashSet<_> = (0..3).map(|_| create_job_tag(&mut rng)).collect();
        assert!(
            tags.len() > 1,
            "create_job_tag must not always produce the same value",
        );
        assert!(tags.iter().all(|tag| tag.len() == 8));
    }

    #[test]
    fn test_get_umask() {
        assert!(matches!(
            get_umask()[..],
            [b'0', b'0'..=b'7', b'0'..=b'7', b'0'..=b'7']
        ));
    }

    #[test]
    fn test_parse_time_from_job_id() {
        use unix_at_parser::chrono::{Local, TimeZone, Utc};
        assert_eq!(
            parse_time_from_job_id(b"at-200010301234.56-12345678"),
            Some(DateTimeTz::Local(
                Local.ymd(2000, 10, 30).and_hms(12, 34, 56)
            )),
        );
        assert_eq!(
            parse_time_from_job_id(b"at-200010301234.56Z-12345678"),
            Some(DateTimeTz::Utc(Utc.ymd(2000, 10, 30).and_hms(12, 34, 56))),
        );
        assert_eq!(parse_time_from_job_id(b"200010301234.56"), None);
    }

    #[test]
    fn test_path_is_name_only() {
        assert!(path_is_name_only("foo"));
        assert!(!path_is_name_only("/foo"));
        assert!(!path_is_name_only("foo/bar"));

        #[cfg(unix)]
        assert!(path_is_name_only("C:foo"));
        #[cfg(windows)]
        assert!(!path_is_name_only("C:foo"));
    }

    #[test]
    fn test_systemd_escape_env_vars() {
        systemd_escape_env_vars(std::env::vars_os());
        assert_eq!(
            systemd_escape_env_vars(vec![("A", "a a"), ("B", "b b")]),
            br#""A=a a" "B=b b""#,
        );
        assert_eq!(
            systemd_escape_env_vars(vec![("A", "a\"a"), ("B", "b\"b")]),
            br#""A=a\"a" "B=b\"b""#,
        );
        assert_eq!(
            systemd_escape_env_vars(vec![("A", "a\\a"), ("B", "b\\b")]),
            br#""A=a\\a" "B=b\\b""#,
        );
        assert_eq!(
            systemd_escape_env_vars(vec![("A", "a\na"), ("B", "b\nb")]),
            br#""A=a\na" "B=b\nb""#,
        );
    }

    #[test]
    fn test_to_systemd_calendar() {
        use unix_at_parser::chrono::{Local, TimeZone, Utc};
        assert_eq!(
            to_systemd_calendar(&DateTimeTz::Local(
                Local.ymd(2000, 10, 30).and_hms(12, 34, 56)
            )),
            "2000-10-30 12:34:56",
        );
        assert_eq!(
            to_systemd_calendar(&DateTimeTz::Utc(Utc.ymd(2000, 10, 30).and_hms(12, 34, 56))),
            "2000-10-30 12:34:56 UTC",
        );
    }
}
