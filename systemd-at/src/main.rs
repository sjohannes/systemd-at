fn main() {
    systemd_at::main().unwrap_or_else(|e| std::process::exit(e.into()));
}
