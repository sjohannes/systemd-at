# unix-at-parser

Parser for POSIX/Unix [`at(1)`](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/at.html) timespec.

Also contains a `touch -t` time parser (used by `at -t`), for convenience.


## Compatibility

| Syntax | POSIX `at` | Debian `at` | unix-at-rs | Note |
| - | - | - | - | - |
| `<hour>.<minute>` | No | Yes | No | |
| `<hour>'<minute>` | No | Yes | No | |
| `TEATIME` | No | Yes | No | Equivalent to `4 PM` |
| Two-digit year | No | Yes | No | |
| `<year><month><day>` | No | Yes | No | |
| `<year>-<month>-<day>` | No | Yes | No | |
| `<month>/<day>/<year>` | No | Yes | No | |
| `<day>.<month>.<year>` | No | Yes | No | |
| `<day> <monthname> [<year>]` | No | Yes | No | |
| Locale-based month/weekday names | Yes | No | No | Only English names supported |
| `NEXT <weekday>` | No | Yes | No | Equivalent to omitting the `NEXT` |
| Mixing date and `NEXT <unit>` | Yes | No | Yes | This is error-prone and highly discouraged (`MONDAY NEXT WEEK` is parsed as `MONDAY + 1 WEEK`) |
| Negative increment | No | Yes | No | |
| `MIN[S]` increment | No | Yes | No | Shorthand for `MINUTE[S]` |
| Specifying more than one increment | No | Yes | No | |
