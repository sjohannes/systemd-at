// Copyright 2020 Johannes Sasongko <sasongko@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use nom::{
    branch::alt,
    bytes::complete::{tag, take_while, take_while_m_n},
    character::is_digit,
    combinator::{all_consuming, map, map_res, opt, value},
    error::make_error,
    sequence::{preceded, terminated, tuple},
    Err::Error,
    IResult,
};

#[derive(Clone, Debug, PartialEq)]
pub struct Time {
    pub h: u8,
    pub m: u8,
}

#[derive(Debug, PartialEq)]
struct TimeError;

impl Time {
    fn try_new24(h: u8, m: u8) -> Result<Time, TimeError> {
        if h <= 23 && m <= 59 {
            Ok(Time { h, m })
        } else {
            Err(TimeError)
        }
    }

    fn try_new12(h: u8, m: u8, ampm: AmPm) -> Result<Time, TimeError> {
        if (1..=12).contains(&h) && m <= 59 {
            Ok(Time {
                h: h - if h == 12 { 12 } else { 0 } + if ampm == AmPm::PM { 12 } else { 0 },
                m,
            })
        } else {
            Err(TimeError)
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
struct Time12 {
    pub h: u8,
    pub m: u8,
}

impl Time12 {
    fn try_new(h: u8, m: u8) -> Result<Time12, TimeError> {
        if (1..=12).contains(&h) && m <= 59 {
            Ok(Time12 { h, m })
        } else {
            Err(TimeError)
        }
    }
}

fn hr24clock_hr_min(i: &[u8]) -> IResult<&[u8], Time> {
    alt((
        time_24h_4digit,
        map_res(hr24clock_hour, |h| Time::try_new24(h, 0)),
    ))(i)
}

fn hr24clock_hour(i: &[u8]) -> IResult<&[u8], u8> {
    digit_1_2(i)
}

fn wallclock_hr_min(i: &[u8]) -> IResult<&[u8], Time12> {
    alt((
        time_12h_4digit,
        map_res(wallclock_hour, |h| Time12::try_new(h, 0)),
    ))(i)
}

fn wallclock_hour(i: &[u8]) -> IResult<&[u8], u8> {
    digit_1_2(i)
}

fn minute(i: &[u8]) -> IResult<&[u8], u8> {
    digit_1_2(i)
}

fn day_number(i: &[u8]) -> IResult<&[u8], u8> {
    digit_1_2(i)
}

fn year_number(i: &[u8]) -> IResult<&[u8], i16> {
    let (i, s) = take_while_m_n(4, 4, is_digit)(i)?;
    Ok((i, to_i16_4(s)))
}

fn inc_number(i: &[u8]) -> IResult<&[u8], u16> {
    let (i, s) = take_while(is_digit)(i)?;
    let s = unsafe { std::str::from_utf8_unchecked(s) };
    Ok((
        i,
        s.parse()
            .map_err(|_| Error(make_error(i, nom::error::ErrorKind::TooLarge)))?,
    ))
}

#[derive(Clone, Debug, PartialEq)]
pub enum TimeZone {
    Utc,
}

fn timezone_name(i: &[u8]) -> IResult<&[u8], TimeZone> {
    map(tag("UTC"), |_| TimeZone::Utc)(i)
}

#[derive(Clone, Debug, PartialEq)]
pub struct Month(pub u8);

fn month_name(i: &[u8]) -> IResult<&[u8], Month> {
    alt((
        value(Month(1), tuple((tag("JAN"), opt(tag("UARY"))))),
        value(Month(2), tuple((tag("FEB"), opt(tag("RUARY"))))),
        value(Month(3), tuple((tag("MAR"), opt(tag("CH"))))),
        value(Month(4), tuple((tag("APR"), opt(tag("IL"))))),
        value(Month(5), tag("MAY")),
        value(Month(6), tuple((tag("JUN"), opt(tag("E"))))),
        value(Month(7), tuple((tag("JUL"), opt(tag("Y"))))),
        value(Month(8), tuple((tag("AUG"), opt(tag("UST"))))),
        value(Month(9), tuple((tag("SEP"), opt(tag("TEMBER"))))),
        value(Month(10), tuple((tag("OCT"), opt(tag("OBER"))))),
        value(Month(11), tuple((tag("NOV"), opt(tag("EMBER"))))),
        value(Month(12), tuple((tag("DEC"), opt(tag("EMBER"))))),
    ))(i)
}

#[derive(Clone, Debug, PartialEq)]
pub enum DayOfWeek {
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
}

fn day_of_week(i: &[u8]) -> IResult<&[u8], DayOfWeek> {
    alt((
        value(DayOfWeek::Sunday, tuple((tag("SUN"), opt(tag("DAY"))))),
        value(DayOfWeek::Monday, tuple((tag("MON"), opt(tag("DAY"))))),
        value(DayOfWeek::Tuesday, tuple((tag("TUE"), opt(tag("SDAY"))))),
        value(
            DayOfWeek::Wednesday,
            tuple((tag("WED"), opt(tag("NESDAY")))),
        ),
        value(DayOfWeek::Thursday, tuple((tag("THU"), opt(tag("RSDAY"))))),
        value(DayOfWeek::Friday, tuple((tag("FRI"), opt(tag("DAY"))))),
        value(DayOfWeek::Saturday, tuple((tag("SAT"), opt(tag("URDAY"))))),
    ))(i)
}

#[derive(Clone, Debug, PartialEq)]
enum AmPm {
    AM,
    PM,
}

fn am_pm(i: &[u8]) -> IResult<&[u8], AmPm> {
    alt((value(AmPm::AM, tag("AM")), value(AmPm::PM, tag("PM"))))(i)
}

#[derive(Clone, Debug, PartialEq)]
pub enum BaseTimeSpec {
    DateTime(Date, Time, Option<TimeZone>),
    Now,
    Time(Time, Option<TimeZone>),
}

pub fn timespec(i: &[u8]) -> IResult<&[u8], (BaseTimeSpec, Option<Increment>)> {
    all_consuming(map(
        tuple((
            space,
            alt((
                value(BaseTimeSpec::Now, tag("NOW")),
                map(tuple((time, space, opt(date))), |((t, tz), _, d)| {
                    if let Some(d) = d {
                        BaseTimeSpec::DateTime(d, t, tz)
                    } else {
                        BaseTimeSpec::Time(t, tz)
                    }
                }),
            )),
            space,
            opt(increment),
            space,
        )),
        |(_, base, _, inc, _)| (base, inc),
    ))(i)
}

fn time(i: &[u8]) -> IResult<&[u8], (Time, Option<TimeZone>)> {
    alt((
        value((Time { h: 12, m: 0 }, None), tag("NOON")),
        value((Time { h: 0, m: 0 }, None), tag("MIDNIGHT")),
        map(
            tuple((
                alt((
                    map_res(
                        tuple((wallclock_hour, space, tag(":"), space, minute, space, am_pm)),
                        |(h, _, _, _, m, _, ampm)| Time::try_new12(h, m, ampm),
                    ),
                    map_res(
                        tuple((hr24clock_hour, space, tag(":"), space, minute)),
                        |(h, _, _, _, m)| Time::try_new24(h, m),
                    ),
                    map_res(
                        tuple((wallclock_hr_min, space, am_pm)),
                        |(Time12 { h, m }, _, ampm)| Time::try_new12(h, m, ampm),
                    ),
                    hr24clock_hr_min,
                )),
                space,
                opt(timezone_name),
            )),
            |(t, _, tz)| (t, tz),
        ),
    ))(i)
}

#[derive(Clone, Debug, PartialEq)]
pub enum Date {
    DayOfWeek(DayOfWeek),
    MonthDay(Month, u8),
    Today,
    Tomorrow,
    YearMonthDay(i16, Month, u8),
}

fn date(i: &[u8]) -> IResult<&[u8], Date> {
    alt((
        value(Date::Today, tag("TODAY")),
        value(Date::Tomorrow, tag("TOMORROW")),
        map(day_of_week, Date::DayOfWeek),
        map(
            tuple((
                month_name,
                space,
                day_number,
                space,
                tag(","),
                space,
                year_number,
            )),
            |(m, _, d, _, _, _, y)| Date::YearMonthDay(y, m, d),
        ),
        map(tuple((month_name, space, day_number)), |(m, _, d)| {
            Date::MonthDay(m, d)
        }),
    ))(i)
}

#[derive(Debug, PartialEq)]
pub struct Increment(pub i16, pub IncPeriod);

fn increment(i: &[u8]) -> IResult<&[u8], Increment> {
    map(
        tuple((
            alt((
                preceded(tuple((tag("+"), space)), inc_number),
                value(1u16, tag("NEXT")),
            )),
            space,
            inc_period,
        )),
        |(n, _, period)| Increment(n as i16, period),
    )(i)
}

#[derive(Clone, Debug, PartialEq)]
pub enum IncPeriod {
    Minute,
    Hour,
    Day,
    Week,
    Month,
    Year,
}

fn inc_period(i: &[u8]) -> IResult<&[u8], IncPeriod> {
    terminated(
        alt((
            value(IncPeriod::Minute, tag("MINUTE")),
            value(IncPeriod::Hour, tag("HOUR")),
            value(IncPeriod::Day, tag("DAY")),
            value(IncPeriod::Week, tag("WEEK")),
            value(IncPeriod::Month, tag("MONTH")),
            value(IncPeriod::Year, tag("YEAR")),
        )),
        opt(tag("S")),
    )(i)
}

// `touch -t` time

type TimeArg = (Option<(Option<u8>, u8)>, u8, u8, u8, u8, u8);

pub fn touch_t(i: &[u8]) -> IResult<&[u8], TimeArg> {
    all_consuming(map_res(
        tuple((
            take_while_m_n(8, 12, is_digit),
            opt(tuple((tag("."), take_while_m_n(2, 2, is_digit)))),
        )),
        |(datetime, second): (&[u8], Option<_>)| {
            let second = match second {
                Some((_, s)) => to_u8_2(s),
                None => 0,
            };
            match datetime.len() {
                8 => Ok((
                    None,
                    to_u8_2(&datetime[0..2]),
                    to_u8_2(&datetime[2..4]),
                    to_u8_2(&datetime[4..6]),
                    to_u8_2(&datetime[6..8]),
                    second,
                )),
                9 => Err(TimeError),
                10 => Ok((
                    Some((None, to_u8_2(&datetime[0..2]))),
                    to_u8_2(&datetime[2..4]),
                    to_u8_2(&datetime[4..6]),
                    to_u8_2(&datetime[6..8]),
                    to_u8_2(&datetime[8..10]),
                    second,
                )),
                11 => Err(TimeError),
                12 => Ok((
                    Some((Some(to_u8_2(&datetime[0..2])), to_u8_2(&datetime[2..4]))),
                    to_u8_2(&datetime[4..6]),
                    to_u8_2(&datetime[6..8]),
                    to_u8_2(&datetime[8..10]),
                    to_u8_2(&datetime[10..12]),
                    second,
                )),
                _ => unreachable!(),
            }
        },
    ))(i)
}

// Helpers

fn space(i: &[u8]) -> IResult<&[u8], &[u8]> {
    take_while(|c| c == b' ')(i)
}

fn digit_1_2(i: &[u8]) -> IResult<&[u8], u8> {
    let (i, s) = take_while_m_n(1, 2, is_digit)(i)?;
    Ok((
        i,
        match s.len() {
            1 => s[0] - b'0',
            _ => to_u8_2(s),
        },
    ))
}

fn time_12h_4digit(i: &[u8]) -> IResult<&[u8], Time12> {
    map_res(take_while_m_n(4, 4, is_digit), |hm: &[u8]| {
        Time12::try_new(to_u8_2(&hm[..2]), to_u8_2(&hm[2..]))
    })(i)
}

fn time_24h_4digit(i: &[u8]) -> IResult<&[u8], Time> {
    map_res(take_while_m_n(4, 4, is_digit), |hm: &[u8]| {
        Time::try_new24(to_u8_2(&hm[..2]), to_u8_2(&hm[2..]))
    })(i)
}

fn to_u8_2(s: &[u8]) -> u8 {
    assert_eq!(s.len(), 2);
    (s[0] - b'0') * 10 + (s[1] - b'0')
}

fn to_i16_4(s: &[u8]) -> i16 {
    assert_eq!(s.len(), 4);
    (s[0] - b'0') as i16 * 1000
        + (s[1] - b'0') as i16 * 100
        + (s[2] - b'0') as i16 * 10
        + (s[3] - b'0') as i16
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_time_ctor() {
        assert_eq!(Time::try_new24(0, 34), Ok(Time { h: 0, m: 34 }));
        assert_eq!(Time::try_new24(12, 34), Ok(Time { h: 12, m: 34 }));
        assert_eq!(Time::try_new24(12, 60), Err(TimeError));
        assert_eq!(Time::try_new24(25, 34), Err(TimeError));
        assert_eq!(Time::try_new12(1, 34, AmPm::AM), Ok(Time { h: 1, m: 34 }));
        assert_eq!(Time::try_new12(1, 34, AmPm::PM), Ok(Time { h: 13, m: 34 }));
        assert_eq!(Time::try_new12(12, 34, AmPm::AM), Ok(Time { h: 0, m: 34 }));
        assert_eq!(Time::try_new12(12, 34, AmPm::PM), Ok(Time { h: 12, m: 34 }));
        assert_eq!(Time::try_new12(0, 34, AmPm::AM), Err(TimeError));
        assert_eq!(Time::try_new12(12, 60, AmPm::AM), Err(TimeError));
        assert_eq!(Time::try_new12(13, 34, AmPm::AM), Err(TimeError));
    }

    #[test]
    fn test_time12_ctor() {
        assert_eq!(Time12::try_new(0, 34), Err(TimeError));
        assert_eq!(Time12::try_new(12, 34), Ok(Time12 { h: 12, m: 34 }));
        assert_eq!(Time12::try_new(12, 60), Err(TimeError));
        assert_eq!(Time12::try_new(25, 34), Err(TimeError));
    }

    #[test]
    fn test_am_pm() {
        const EMPTY: &[u8] = &[];
        assert_eq!(am_pm(b"AM"), Ok((EMPTY, AmPm::AM)));
        assert_eq!(am_pm(b"PM"), Ok((EMPTY, AmPm::PM)));
        assert!(all_consuming(am_pm)(b"MM").is_err());
    }

    #[test]
    fn test_day_of_week() {
        const EMPTY: &[u8] = &[];
        assert_eq!(day_of_week(b"SUNDAY"), Ok((EMPTY, DayOfWeek::Sunday)));
        assert_eq!(day_of_week(b"MONDAY"), Ok((EMPTY, DayOfWeek::Monday)));
        assert_eq!(day_of_week(b"TUESDAY"), Ok((EMPTY, DayOfWeek::Tuesday)));
        assert_eq!(day_of_week(b"WEDNESDAY"), Ok((EMPTY, DayOfWeek::Wednesday)));
        assert_eq!(day_of_week(b"THURSDAY"), Ok((EMPTY, DayOfWeek::Thursday)));
        assert_eq!(day_of_week(b"FRIDAY"), Ok((EMPTY, DayOfWeek::Friday)));
        assert_eq!(day_of_week(b"SATURDAY"), Ok((EMPTY, DayOfWeek::Saturday)));
        assert_eq!(day_of_week(b"SUN"), Ok((EMPTY, DayOfWeek::Sunday)));
        assert_eq!(day_of_week(b"MON"), Ok((EMPTY, DayOfWeek::Monday)));
        assert_eq!(day_of_week(b"TUE"), Ok((EMPTY, DayOfWeek::Tuesday)));
        assert_eq!(day_of_week(b"WED"), Ok((EMPTY, DayOfWeek::Wednesday)));
        assert_eq!(day_of_week(b"THU"), Ok((EMPTY, DayOfWeek::Thursday)));
        assert_eq!(day_of_week(b"FRI"), Ok((EMPTY, DayOfWeek::Friday)));
        assert_eq!(day_of_week(b"SAT"), Ok((EMPTY, DayOfWeek::Saturday)));
        assert!(all_consuming(day_of_week)(b"AAA").is_err());
        assert_eq!(day_of_week(b"SUND"), Ok((&b"D"[..], DayOfWeek::Sunday)));
    }

    #[test]
    fn test_hr24clock_hr_min() {
        const EMPTY: &[u8] = &[];
        assert_eq!(hr24clock_hr_min(b"0"), Ok((EMPTY, Time { h: 0, m: 0 })));
        assert_eq!(hr24clock_hr_min(b"12"), Ok((EMPTY, Time { h: 12, m: 0 })));
        assert_eq!(hr24clock_hr_min(b"13"), Ok((EMPTY, Time { h: 13, m: 0 })));
        assert!(all_consuming(hr24clock_hr_min)(b"24").is_err());
        assert!(all_consuming(hr24clock_hr_min)(b"123").is_err());
        assert_eq!(
            hr24clock_hr_min(b"1234"),
            Ok((EMPTY, Time { h: 12, m: 34 })),
        );
        assert!(all_consuming(hr24clock_hr_min)(b"2400").is_err());
        assert!(all_consuming(hr24clock_hr_min)(b"1060").is_err());
        assert!(all_consuming(hr24clock_hr_min)(b"abcd").is_err());
        assert!(all_consuming(hr24clock_hr_min)(b"12345").is_err());
    }

    #[test]
    fn test_month_name() {
        const EMPTY: &[u8] = &[];
        assert_eq!(month_name(b"JANUARY"), Ok((EMPTY, Month(1))));
        assert_eq!(month_name(b"FEBRUARY"), Ok((EMPTY, Month(2))));
        assert_eq!(month_name(b"MARCH"), Ok((EMPTY, Month(3))));
        assert_eq!(month_name(b"APRIL"), Ok((EMPTY, Month(4))));
        assert_eq!(month_name(b"MAY"), Ok((EMPTY, Month(5))));
        assert_eq!(month_name(b"JUNE"), Ok((EMPTY, Month(6))));
        assert_eq!(month_name(b"JULY"), Ok((EMPTY, Month(7))));
        assert_eq!(month_name(b"AUGUST"), Ok((EMPTY, Month(8))));
        assert_eq!(month_name(b"SEPTEMBER"), Ok((EMPTY, Month(9))));
        assert_eq!(month_name(b"OCTOBER"), Ok((EMPTY, Month(10))));
        assert_eq!(month_name(b"NOVEMBER"), Ok((EMPTY, Month(11))));
        assert_eq!(month_name(b"DECEMBER"), Ok((EMPTY, Month(12))));
        assert_eq!(month_name(b"JAN"), Ok((EMPTY, Month(1))));
        assert_eq!(month_name(b"FEB"), Ok((EMPTY, Month(2))));
        assert_eq!(month_name(b"MAR"), Ok((EMPTY, Month(3))));
        assert_eq!(month_name(b"APR"), Ok((EMPTY, Month(4))));
        assert_eq!(month_name(b"MAY"), Ok((EMPTY, Month(5))));
        assert_eq!(month_name(b"JUN"), Ok((EMPTY, Month(6))));
        assert_eq!(month_name(b"JUL"), Ok((EMPTY, Month(7))));
        assert_eq!(month_name(b"AUG"), Ok((EMPTY, Month(8))));
        assert_eq!(month_name(b"SEP"), Ok((EMPTY, Month(9))));
        assert_eq!(month_name(b"OCT"), Ok((EMPTY, Month(10))));
        assert_eq!(month_name(b"NOV"), Ok((EMPTY, Month(11))));
        assert_eq!(month_name(b"DEC"), Ok((EMPTY, Month(12))));
        assert!(all_consuming(month_name)(b"AAA").is_err());
        assert_eq!(month_name(b"JANU"), Ok((&b"U"[..], Month(1))));
    }

    #[test]
    fn test_space() {
        const EMPTY: &[u8] = &[];
        assert_eq!(space(b""), Ok((EMPTY, EMPTY)));
        assert_eq!(space(b"  "), Ok((EMPTY, &b"  "[..])));
        assert_eq!(space(b"  A "), Ok((&b"A "[..], &b"  "[..])));
    }

    #[test]
    fn test_time() {
        const EMPTY: &[u8] = &[];
        assert_eq!(time(b"NOON"), Ok((EMPTY, (Time { h: 12, m: 0 }, None))));
        assert_eq!(time(b"MIDNIGHT"), Ok((EMPTY, (Time { h: 0, m: 0 }, None))));
        // H:M AMPM
        assert!(all_consuming(time)(b"0:34 AM").is_err());
        assert_eq!(time(b"1:34 PM"), Ok((EMPTY, (Time { h: 13, m: 34 }, None))));
        assert_eq!(time(b"12:34 AM"), Ok((EMPTY, (Time { h: 0, m: 34 }, None))));
        // H:M
        assert_eq!(time(b"0:34"), Ok((EMPTY, (Time { h: 0, m: 34 }, None))));
        assert_eq!(time(b"13:34"), Ok((EMPTY, (Time { h: 13, m: 34 }, None))));
        assert!(all_consuming(time)(b"24:00").is_err());
        // HHMM
        assert_eq!(time(b"0034"), Ok((EMPTY, (Time { h: 0, m: 34 }, None))));
        assert_eq!(time(b"1334"), Ok((EMPTY, (Time { h: 13, m: 34 }, None))));
        assert!(all_consuming(time)(b"2400").is_err());
        // H
        assert_eq!(time(b"0"), Ok((EMPTY, (Time { h: 0, m: 0 }, None))));
        assert_eq!(time(b"13"), Ok((EMPTY, (Time { h: 13, m: 0 }, None))));
        assert!(all_consuming(time)(b"24").is_err());
        // Weird
        assert!(all_consuming(time)(b"012").is_err());
        // Timezone
        assert_eq!(
            time(b"1234 UTC"),
            Ok((EMPTY, (Time { h: 12, m: 34 }, Some(TimeZone::Utc)))),
        );
    }

    #[test]
    fn test_timespec() {
        const EMPTY: &[u8] = &[];
        assert_eq!(timespec(b"NOW"), Ok((EMPTY, (BaseTimeSpec::Now, None))));
        assert_eq!(
            timespec(b"1234 OCT 30, 2000"),
            Ok((
                EMPTY,
                (
                    BaseTimeSpec::DateTime(
                        Date::YearMonthDay(2000, Month(10), 30),
                        Time { h: 12, m: 34 },
                        None,
                    ),
                    None
                )
            )),
        );
        assert_eq!(
            timespec(b"1234"),
            Ok((
                EMPTY,
                (BaseTimeSpec::Time(Time { h: 12, m: 34 }, None), None),
            )),
        );
        assert_eq!(
            timespec(b"2334 + 1 HOUR"),
            Ok((
                EMPTY,
                (
                    BaseTimeSpec::Time(Time { h: 23, m: 34 }, None),
                    Some(Increment(1, IncPeriod::Hour)),
                ),
            )),
        );
    }

    #[test]
    fn test_timezone_name() {
        const EMPTY: &[u8] = &[];
        assert_eq!(timezone_name(b"UTC"), Ok((EMPTY, TimeZone::Utc)));
        assert!(timezone_name(b"!").is_err());
    }

    #[test]
    fn test_wallclock_hr_min() {
        const EMPTY: &[u8] = &[];
        assert!(all_consuming(wallclock_hr_min)(b"0").is_err());
        assert_eq!(wallclock_hr_min(b"1"), Ok((EMPTY, Time12 { h: 1, m: 0 })));
        assert_eq!(wallclock_hr_min(b"12"), Ok((EMPTY, Time12 { h: 12, m: 0 })));
        assert!(all_consuming(wallclock_hr_min)(b"13").is_err());
        assert!(all_consuming(wallclock_hr_min)(b"123").is_err());
        assert_eq!(
            wallclock_hr_min(b"1234"),
            Ok((EMPTY, Time12 { h: 12, m: 34 })),
        );
        assert!(all_consuming(wallclock_hr_min)(b"1300").is_err());
        assert!(all_consuming(wallclock_hr_min)(b"1060").is_err());
        assert!(all_consuming(wallclock_hr_min)(b"abcd").is_err());
        assert!(all_consuming(wallclock_hr_min)(b"12345").is_err());
    }

    #[test]
    fn test_year_number() {
        const EMPTY: &[u8] = &[];
        assert_eq!(year_number(b"1234"), Ok((EMPTY, 1234)));
        assert!(year_number(b"123").is_err());
    }

    #[test]
    fn test_touch_t() {
        const EMPTY: &[u8] = &[];
        assert_eq!(
            touch_t(b"200010301234.56"),
            Ok((EMPTY, (Some((Some(20), 00)), 10, 30, 12, 34, 56))),
        );
        assert_eq!(
            touch_t(b"200010301234"),
            Ok((EMPTY, (Some((Some(20), 00)), 10, 30, 12, 34, 00))),
        );
        assert_eq!(
            touch_t(b"0010301234"),
            Ok((EMPTY, (Some((None, 00)), 10, 30, 12, 34, 00))),
        );
        assert_eq!(
            touch_t(b"10301234"),
            Ok((EMPTY, (None, 10, 30, 12, 34, 00))),
        );
    }
}
