// Copyright 2020 Johannes Sasongko <sasongko@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Unix `at(1)` timespec parser.
//!
//! For the grammar, see the [POSIX `at(1)` specification][posix-at].
//! The only difference is that only English month and day names are supported.
//!
//! [posix-at]: https://pubs.opengroup.org/onlinepubs/9699919799/utilities/at.html
//!
//! # Example
//!
//! ```
//! use unix_at_parser::{chrono, parse_timespec, DateTimeTz};
//! parse_timespec(
//!     b"12:34 October 30, 2000 + 2 weeks",
//!     &DateTimeTz::Local(chrono::Local::now()),
//! );
//! ```

mod parser;

pub use chrono;
use chrono::{Datelike, Duration, Local, TimeZone, Utc};

/// Union of local and UTC datetime types.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum DateTimeTz {
    Local(chrono::DateTime<Local>),
    Utc(chrono::DateTime<Utc>),
}

impl DateTimeTz {
    fn add_duration(self, dur: Duration) -> DateTimeTz {
        match self {
            DateTimeTz::Local(dt) => DateTimeTz::Local(dt + dur),
            DateTimeTz::Utc(dt) => DateTimeTz::Utc(dt + dur),
        }
    }

    fn add_months(self, n: i16) -> DateTimeTz {
        let add_to = |y: i32, m_idx: u32| -> (i32, u32) {
            let m_idx = m_idx as i32 + n as i32;
            let y_diff = m_idx / 12;
            let m_idx = m_idx % 12;
            let y = y + y_diff;
            let m = m_idx as u32 + 1;
            (y, m)
        };
        // When going to a month that doesn't have the corresponding day, use
        // the 1st of the next month
        match self {
            DateTimeTz::Local(dt) => DateTimeTz::Local({
                let (y, m) = add_to(dt.year(), dt.month0());
                Local
                    .ymd_opt(y, m, dt.day())
                    .latest()
                    .unwrap_or_else(|| Local.ymd_opt(y, m + 1, 1).latest().unwrap())
                    .and_time(dt.time())
                    .unwrap()
            }),
            DateTimeTz::Utc(dt) => DateTimeTz::Utc({
                let (y, m) = add_to(dt.year(), dt.month0());
                Utc.ymd_opt(y, m, dt.day())
                    .latest()
                    .unwrap_or_else(|| Utc.ymd_opt(y, m + 1, 1).latest().unwrap())
                    .and_time(dt.time())
                    .unwrap()
            }),
        }
    }

    fn add_years(self, n: i16) -> DateTimeTz {
        // When going from 29 Feb of a leap year to a non-leap year, use 1 Mar
        match self {
            DateTimeTz::Local(dt) => DateTimeTz::Local({
                let y = dt.year() + n as i32;
                dt.with_year(y)
                    .unwrap_or_else(|| Local.ymd(y, 3, 1).and_time(dt.time()).unwrap())
            }),
            DateTimeTz::Utc(dt) => DateTimeTz::Utc({
                let y = dt.year() + n as i32;
                dt.with_year(y)
                    .unwrap_or_else(|| Utc.ymd(y, 3, 1).and_time(dt.time()).unwrap())
            }),
        }
    }
}

/// Error while parsing a timespec.
#[derive(Debug, PartialEq)]
pub struct ParseError(String);

impl std::error::Error for ParseError {}

impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.0)
    }
}

/// Parses a timespec.
pub fn parse_timespec(timespec: &[u8], now: &DateTimeTz) -> Result<DateTimeTz, ParseError> {
    use std::borrow::Cow;
    let get_now_utc = || match now {
        DateTimeTz::Local(dt) => Cow::Owned(dt.with_timezone(&Utc)),
        DateTimeTz::Utc(dt) => Cow::Borrowed(dt),
    };
    let get_now_local = || match now {
        DateTimeTz::Local(dt) => Cow::Borrowed(dt),
        DateTimeTz::Utc(dt) => Cow::Owned(dt.with_timezone(&Local)),
    };
    let get_is_utc = |tz| match tz {
        Some(parser::TimeZone::Utc) => true,
        None => false,
    };
    let s = timespec.to_ascii_uppercase();
    let (_, (base, inc)) = parser::timespec(&s).map_err(|e| ParseError(format!("{}", e)))?;
    use parser::BaseTimeSpec;
    let base: DateTimeTz = match base {
        BaseTimeSpec::Now => *now,
        BaseTimeSpec::Time(time, tz) => {
            if get_is_utc(tz) {
                let now = get_now_utc();
                let dt = now.date().and_hms(time.h as u32, time.m as u32, 0);
                DateTimeTz::Utc(if &dt >= &now {
                    dt
                } else {
                    dt + Duration::days(1)
                })
            } else {
                let now = get_now_local();
                let dt = now.date().and_hms(time.h as u32, time.m as u32, 0);
                DateTimeTz::Local(if &dt >= &now {
                    dt
                } else {
                    dt + Duration::days(1)
                })
            }
        }
        BaseTimeSpec::DateTime(date, time, tz) => {
            use parser::Date;
            let is_utc = get_is_utc(tz);
            let dt = match date {
                Date::Today => {
                    let d = if is_utc {
                        get_now_utc().date().naive_utc()
                    } else {
                        get_now_local().date().naive_local()
                    };
                    d.and_hms(time.h as u32, time.m as u32, 0)
                }
                Date::Tomorrow => {
                    let d = if is_utc {
                        get_now_utc().date().naive_utc()
                    } else {
                        get_now_local().date().naive_local()
                    };
                    d.and_hms(time.h as u32, time.m as u32, 0) + Duration::days(1)
                }
                Date::YearMonthDay(y, m, d) => {
                    let d = if is_utc {
                        Utc.ymd_opt(y as i32, m.0 as u32, d as u32)
                            .latest()
                            .ok_or_else(|| ParseError(String::from("invalid date")))?
                            .naive_utc()
                    } else {
                        Local
                            .ymd_opt(y as i32, m.0 as u32, d as u32)
                            .latest()
                            .ok_or_else(|| ParseError(String::from("invalid date")))?
                            .naive_local()
                    };
                    d.and_hms(time.h as u32, time.m as u32, 0)
                }
                Date::MonthDay(m, d) => {
                    let (curr_year, curr_month) = if is_utc {
                        let now = get_now_utc();
                        (now.year(), now.month())
                    } else {
                        let now = get_now_local();
                        (now.year(), now.month())
                    };
                    #[allow(clippy::collapsible_if)]
                    let d = if m.0 >= curr_month as u8 {
                        if is_utc {
                            Utc.ymd_opt(curr_year as i32, m.0 as u32, d as u32)
                                .latest()
                                .ok_or_else(|| ParseError(String::from("invalid date")))?
                                .naive_utc()
                        } else {
                            Local
                                .ymd_opt(curr_year as i32, m.0 as u32, d as u32)
                                .latest()
                                .ok_or_else(|| ParseError(String::from("invalid date")))?
                                .naive_utc()
                        }
                    } else {
                        if is_utc {
                            Utc.ymd_opt((curr_year + 1) as i32, m.0 as u32, d as u32)
                                .latest()
                                .ok_or_else(|| ParseError(String::from("invalid date")))?
                                .naive_utc()
                        } else {
                            Local
                                .ymd_opt((curr_year + 1) as i32, m.0 as u32, d as u32)
                                .latest()
                                .ok_or_else(|| ParseError(String::from("invalid date")))?
                                .naive_utc()
                        }
                    };
                    d.and_hms(time.h as u32, time.m as u32, 0)
                }
                Date::DayOfWeek(wd) => {
                    let now = if is_utc {
                        get_now_utc().naive_utc()
                    } else {
                        get_now_local().naive_local()
                    };
                    let delta = wd as i8 - now.weekday().num_days_from_sunday() as i8;
                    let delta = if delta <= 0 { delta + 7 } else { delta };
                    now + Duration::days(delta as i64)
                }
            };
            if is_utc {
                DateTimeTz::Utc(
                    Utc.from_local_datetime(&dt)
                        .latest()
                        .ok_or_else(|| ParseError(String::from("Invalid datetime")))?,
                )
            } else {
                DateTimeTz::Local(
                    Local
                        .from_local_datetime(&dt)
                        .latest()
                        .ok_or_else(|| ParseError(String::from("Invalid datetime")))?,
                )
            }
        }
    };
    use parser::{IncPeriod, Increment};
    let result = match inc {
        Some(Increment(n, u)) => match u {
            IncPeriod::Minute => base.add_duration(Duration::minutes(n as i64)),
            IncPeriod::Hour => base.add_duration(Duration::hours(n as i64)),
            IncPeriod::Day => base.add_duration(Duration::days(n as i64)),
            IncPeriod::Week => base.add_duration(Duration::weeks(n as i64)),
            IncPeriod::Month => base.add_months(n),
            IncPeriod::Year => base.add_years(n),
        },
        None => base,
    };
    Ok(result)
}

/// Parses a `touch -t` datetime.
pub fn parse_touch_t(time_arg: &[u8], now: &DateTimeTz) -> Result<DateTimeTz, ParseError> {
    let (_, (year, month, day, hour, minute, second)) =
        parser::touch_t(time_arg).map_err(|e| ParseError(format!("{}", e)))?;
    let year = match year {
        Some((Some(c), y)) => c as i32 * 100 + y as i32,
        Some((None, y)) => (if y >= 69 { 1900 } else { 2000 }) + y as i32,
        None => match now {
            DateTimeTz::Local(dt) => dt.year(),
            DateTimeTz::Utc(dt) => dt.year(),
        },
    };
    Ok(match now {
        DateTimeTz::Local(_) => DateTimeTz::Local(
            chrono::Local
                .ymd_opt(year, month as u32, day as u32)
                .and_hms_opt(hour as u32, minute as u32, second as u32)
                .latest()
                .ok_or_else(|| ParseError(String::from("Invalid time_arg")))?,
        ),
        DateTimeTz::Utc(_) => DateTimeTz::Utc(
            chrono::Utc
                .ymd_opt(year, month as u32, day as u32)
                .and_hms_opt(hour as u32, minute as u32, second as u32)
                .latest()
                .ok_or_else(|| ParseError(String::from("Invalid time_arg")))?,
        ),
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use once_cell::sync::Lazy;

    static LOCAL: Lazy<DateTimeTz> =
        Lazy::new(|| DateTimeTz::Local(Local.ymd(2000, 10, 30).and_hms(12, 34, 0)));
    static UTC: Lazy<DateTimeTz> =
        Lazy::new(|| DateTimeTz::Utc(Utc.ymd(2000, 10, 30).and_hms(12, 34, 0)));

    #[test]
    fn test_parse_now() {
        assert_eq!(parse_timespec(b"noW", &LOCAL), Ok(*LOCAL));
        assert_eq!(parse_timespec(b"noW", &UTC), Ok(*UTC));
    }

    #[test]
    fn test_parse_time() {
        assert_eq!(
            parse_timespec(b"1233", &LOCAL),
            Ok(DateTimeTz::Local(
                Local.ymd(2000, 10, 31).and_hms(12, 33, 0)
            )),
        );
        assert_eq!(
            parse_timespec(b"1233 utC", &UTC),
            Ok(DateTimeTz::Utc(Utc.ymd(2000, 10, 31).and_hms(12, 33, 0))),
        );
        // NOTE: The time == now.time case is undefined, so we don't test it.
        assert_eq!(
            parse_timespec(b"1235", &LOCAL),
            Ok(DateTimeTz::Local(
                Local.ymd(2000, 10, 30).and_hms(12, 35, 0)
            )),
        );
        assert_eq!(
            parse_timespec(b"1235 utC", &UTC),
            Ok(DateTimeTz::Utc(Utc.ymd(2000, 10, 30).and_hms(12, 35, 0))),
        );
    }

    #[test]
    fn test_parse_datetime_today() {
        assert_eq!(
            parse_timespec(b"1020 todaY", &LOCAL),
            Ok(DateTimeTz::Local(
                Local.ymd(2000, 10, 30).and_hms(10, 20, 0)
            )),
        );
        assert_eq!(
            parse_timespec(b"1020 utC todaY", &UTC),
            Ok(DateTimeTz::Utc(Utc.ymd(2000, 10, 30).and_hms(10, 20, 0))),
        );
    }

    #[test]
    fn test_parse_datetime_tomorrow() {
        assert_eq!(
            parse_timespec(b"1020 tomorroW", &LOCAL),
            Ok(DateTimeTz::Local(
                Local.ymd(2000, 10, 31).and_hms(10, 20, 0)
            )),
        );
        assert_eq!(
            parse_timespec(b"1020 utC tomorroW", &UTC),
            Ok(DateTimeTz::Utc(Utc.ymd(2000, 10, 31).and_hms(10, 20, 0))),
        );
    }

    #[test]
    fn test_parse_datetime_yearmonthdate() {
        assert_eq!(parse_timespec(b"1234 ocT 30, 2000", &LOCAL), Ok(*LOCAL));
        assert_eq!(parse_timespec(b"1234 utC ocT 30, 2000", &UTC), Ok(*UTC));
    }

    #[test]
    fn test_parse_datetime_monthdate() {
        assert_eq!(
            parse_timespec(b"1234 seP 23", &LOCAL),
            Ok(DateTimeTz::Local(Local.ymd(2001, 9, 23).and_hms(12, 34, 0))),
        );
        assert_eq!(
            parse_timespec(b"1234 utC seP 23", &UTC),
            Ok(DateTimeTz::Utc(Utc.ymd(2001, 9, 23).and_hms(12, 34, 0))),
        );
        // The "roll over to next month" case only compares the month, not the date
        assert_eq!(
            parse_timespec(b"1234 ocT 23", &LOCAL),
            Ok(DateTimeTz::Local(
                Local.ymd(2000, 10, 23).and_hms(12, 34, 0)
            )),
        );
        assert_eq!(
            parse_timespec(b"1234 utC ocT 23", &UTC),
            Ok(DateTimeTz::Utc(Utc.ymd(2000, 10, 23).and_hms(12, 34, 0))),
        );
    }

    #[test]
    fn test_parse_datetime_dayofweek() {
        assert_eq!(
            parse_timespec(b"1234 sunday", &LOCAL),
            Ok(DateTimeTz::Local(Local.ymd(2000, 11, 5).and_hms(12, 34, 0))),
        );
        assert_eq!(
            parse_timespec(b"1234 utc sunday", &UTC),
            Ok(DateTimeTz::Utc(Utc.ymd(2000, 11, 5).and_hms(12, 34, 0))),
        );
    }

    #[test]
    fn test_parse_increment() {
        // Minutes, hours, days
        assert_eq!(
            parse_timespec(b"1234 oct 30, 2000 + 30 minuteS", &LOCAL),
            Ok(DateTimeTz::Local(Local.ymd(2000, 10, 30).and_hms(13, 4, 0))),
        );
        assert_eq!(
            parse_timespec(b"1234 oct 30, 2000 + 30 hourS", &LOCAL),
            Ok(DateTimeTz::Local(
                Local.ymd(2000, 10, 31).and_hms(18, 34, 0)
            )),
        );
        assert_eq!(
            parse_timespec(b"1234 oct 30, 2000 + 30 dayS", &LOCAL),
            Ok(DateTimeTz::Local(
                Local.ymd(2000, 11, 29).and_hms(12, 34, 0)
            )),
        );

        // Months, years
        assert_eq!(
            parse_timespec(b"1234 jan 31, 2000 + 3 monthS", &LOCAL),
            Ok(DateTimeTz::Local(Local.ymd(2000, 5, 1).and_hms(12, 34, 0))),
        );
        assert_eq!(
            parse_timespec(b"1234 jan 31, 2000 + 13 monthS", &LOCAL),
            Ok(DateTimeTz::Local(Local.ymd(2001, 3, 1).and_hms(12, 34, 0))),
        );
        assert_eq!(
            parse_timespec(b"1234 feb 29, 2000 + 3 yearS", &LOCAL),
            Ok(DateTimeTz::Local(Local.ymd(2003, 3, 1).and_hms(12, 34, 0))),
        );

        // UTC preserved
        assert_eq!(
            parse_timespec(b"1234 utc oct 30, 2000 + 1 minutE", &UTC),
            Ok(DateTimeTz::Utc(Utc.ymd(2000, 10, 30).and_hms(12, 35, 0))),
        );
    }

    #[test]
    fn test_mix_tz() {
        assert!(parse_timespec(b"1234 utc", &LOCAL).is_ok());
        assert!(parse_timespec(b"1234", &UTC).is_ok());
    }

    #[test]
    fn test_compatibility() {
        // These must match the compatibility table in README.md
        // (except any that can't be tested)
        assert!(parse_timespec(b"12.34", &LOCAL).is_err());
        assert!(parse_timespec(b"12'34", &LOCAL).is_err());
        assert!(parse_timespec(b"teatime", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 20001030", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 2000-10-30", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 10/30/2000", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 30.10.2000", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 30 oct 2000", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 next monday", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 monday next week", &LOCAL).is_ok());
        assert!(parse_timespec(b"1234 oct 30, 9999 - 1 hour", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 + 1 min", &LOCAL).is_err());
        assert!(parse_timespec(b"1234 + 1 hour + 1 hour", &LOCAL).is_err());
    }

    #[test]
    fn test_parse_touch_t() {
        // Full
        assert_eq!(
            parse_touch_t(b"200010301234.56", &LOCAL),
            Ok(DateTimeTz::Local(
                chrono::Local.ymd(2000, 10, 30).and_hms(12, 34, 56)
            )),
        );

        // No seconds
        assert_eq!(
            parse_touch_t(b"200010301234", &LOCAL),
            Ok(DateTimeTz::Local(
                chrono::Local.ymd(2000, 10, 30).and_hms(12, 34, 00)
            )),
        );

        // No century
        assert_eq!(
            parse_touch_t(b"0010301234", &LOCAL),
            Ok(DateTimeTz::Local(
                chrono::Local.ymd(2000, 10, 30).and_hms(12, 34, 00)
            )),
        );
        assert_eq!(
            parse_touch_t(b"6810301234", &LOCAL),
            Ok(DateTimeTz::Local(
                chrono::Local.ymd(2068, 10, 30).and_hms(12, 34, 00)
            )),
        );
        assert_eq!(
            parse_touch_t(b"6910301234", &LOCAL),
            Ok(DateTimeTz::Local(
                chrono::Local.ymd(1969, 10, 30).and_hms(12, 34, 00)
            )),
        );
        assert_eq!(
            parse_touch_t(b"9910301234", &LOCAL),
            Ok(DateTimeTz::Local(
                chrono::Local.ymd(1999, 10, 30).and_hms(12, 34, 00)
            )),
        );

        // No year
        assert_eq!(
            parse_touch_t(b"01010000", &LOCAL),
            Ok(DateTimeTz::Local(
                chrono::Local.ymd(2000, 01, 01).and_hms(00, 00, 00)
            ))
        );

        // Invalid datetime
        assert!(parse_touch_t(b"200010321234.56", &LOCAL).is_err());
        assert!(parse_touch_t(b"200010301234.61", &LOCAL).is_err());
        assert!(parse_touch_t(b"1111111", &LOCAL).is_err());
        assert!(parse_touch_t(b"111111111", &LOCAL).is_err());
        assert!(parse_touch_t(b"11111111111", &LOCAL).is_err());
        assert!(parse_touch_t(b"1111111111111", &LOCAL).is_err());
    }
}
